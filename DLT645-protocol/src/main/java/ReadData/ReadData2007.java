package ReadData;

import DataIdentify.DataIdentify2007;
import OtherFuction.GetTime;
import OtherFuction.ToHex;
import OtherFuction.check;
import constant.RedisCons;
import utils.DateUtil;
import utils.PropertiesUtils;
import utils.RedisPool;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadData2007 {

	private static Logger log = LoggerFactory.getLogger(ReadData2007.class);

	private int lengthOfData;
	private int doc;// 确定小数点
	private String Ammeter_Address;

	public double Read_data(byte[] bytes, String clientIp, int ClientPort) {
		Ammeter_Address = check.GetBCDAddress(bytes);
		if (bytes[0] == 0x00) {
			log.info("Client " + " logout" + "\n");
//			return "logout";
			return -1;
		}
		if (bytes[0] == 0x11) {
			log.info("This Is Heart bit" + "\n");
//			return "bit";
			return -2;
		}
//		log.info(Arrays.toString(bytes));
		boolean check = OtherFuction.check.checkData(bytes);
//		log.info(check + "");
		if (!check) {
//			return "checking_error";
			return -3;
		} else {
			int[] read_ints = new int[bytes.length];
			for (int i = 0; i < bytes.length; i++) {
				if ((int) bytes[i] > 0) {
					read_ints[i] = (int) bytes[i];
				} else {
					read_ints[i] = (int) bytes[i] + 256;
				}
			}
			int j = 0;
//			if (read_ints[j] == 0x68) {
//				System.out.print("start_byte  ");
//			}
			switch (read_ints[8])// read_int[8]为控制码
			{
			case 0xD1:
//				return "error";
				return -4;
			case 0xD2:
//				return "error";
				return -4;
			// 读数据且无后续帧
			case 0x91:
//				System.out.print("normally " + "\n");
				lengthOfData = read_ints[9];
				byte[] type = new byte[4];
				byte[] data = new byte[lengthOfData - 4];
				for (int t = 0; t < 4; t++) {
					type[t] = (byte) (read_ints[10 + t] - 0x33);
				}
				for (int d = 0; d < lengthOfData - 4; d++) {
					data[d] = (byte) (read_ints[14 + d] - 0x33);
				}

//				for (int t = 0; t < type.length; t++) {
//					log.info(type[t] + "");
//				}

				DataIdentify2007 dataIdentify = new DataIdentify2007();
				String datatpye = dataIdentify.identifyname.get(Arrays.toString(type));
				doc = dataIdentify.doc.get(Arrays.toString(type));
				int value = 0;
				for (int v = 0; v < lengthOfData - 4; v++) {
					value = v + (data[v] * 10 / 16 + (data[v]) % 16) * 100 ^ v;
				}
				double dataresult = ((double) value) / (10 ^ doc);
				InetAddress address = null;
				try {
					address = InetAddress.getLocalHost();
				} catch (UnknownHostException e1) {
//					log.debug("#首次获取本服务ip异常,");
					try {
						address= Inet4Address.getLocalHost();
					} catch (UnknownHostException e) {
						address=null;
//						log.debug("#2次获取本服务ip异常");
					}
				}
				String serverIp=address==null?"168.168.168.168":address.getHostAddress();
				String key = RedisCons.collect +  serverIp+ ":"
						+ PropertiesUtils.getProperty("server.port", "6000") + "_C:" + clientIp + ":" + ClientPort
						+ "_T:" + datatpye;
				String nowDate = DateUtil.getSdf("yyyyMMddHHmmss").format(new Date());
				RedisPool.hset(key, nowDate, dataresult + "",
						Integer.parseInt(PropertiesUtils.getProperty("collectionDataTimeout", "86400")));
				log.debug("采集数据缓存成功，#key:[{}],#value:[{}]", key, dataresult);
//				String str_addr = ToHex.ToHex(bytes);
//				log.debug(GetTime.NowTime() + " Received  " + datatpye + " from " + Ammeter_Address + "  Client : "
//						+ str_addr + "\n");
//				log.debug("*******************" + datatpye + " : " + dataresult + "***********************\n");
				// WriteDataToDatabase writeToDataBase=new WriteDataToDatabase();
//                    writeToDataBase.write(datatpye,dataresult,Ammeter_Address);
//				log.debug("client " + Ammeter_Address + " write " + datatpye + " " + dataresult);
				return dataresult;
			}
		}
//		return "complete";
		return 0;
	}
}
