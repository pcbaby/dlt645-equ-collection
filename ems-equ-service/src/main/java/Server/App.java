package Server;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import DataIdentify.DataIdentify2007;
import OtherFuction.ToHex;
import constant.RedisCons;

/**
 * 
 * 启动类
 * 
 * @author ack @date Oct 27, 2019
 *
 */
public class App {

	private static Logger log = LoggerFactory.getLogger(App.class);

	/**
	 * 已经初始化获取地址域的电表设备
	 */
	static Set<String> initEquIdSet = ConcurrentHashMap.newKeySet();
	/**
	 * 已经初始化获取地址域，并已经开始采集的电表设备
	 */
	static Set<String> runningEquIdSet = ConcurrentHashMap.newKeySet();
	/**
	 * 已经初始化获取地址域，并已经开始采集的电表设备
	 */
	static Map<String, byte[]> runningEquIdAddressMap = new ConcurrentHashMap<String, byte[]>();

	public static void main(String[] args) throws InterruptedException {
		log.info("#采集监听服务-启动开始");
		new Server().start();
		while (1 == 1) {
			for (Iterator iterator = Server.connectedEquId2Ctx.keySet().iterator(); iterator.hasNext();) {
				String equId = (String) iterator.next();
				if (!runningEquIdSet.contains(equId)) {
					log.info("#新电表加入采集作业:[{}]", equId);
					// 获取电表的地址域
					byte[] address = runningEquIdAddressMap.get(equId);
					// RedisPool.getByte((RedisCons.address + equId).getBytes());
					if (address == null || address.length <= 0) {
						log.info("#电表：[{}]的地址域未初始化，发送获取地址域的帧", equId);
						Sender.send(null, equId,
								new byte[] { (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0x68,
										(byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA,
										(byte) 0x68, (byte) 0x13, (byte) 0x00, (byte) 0xDF, (byte) 0x16, });
						continue;
					}
					log.info("#电表地址域已有，直接采集各项数据，#key:[{}]，#address:[{}]", RedisCons.address + equId,
							ToHex.ToHex(address));
					runningEquIdSet.add(equId);
					new Sender(equId, address, DataIdentify2007.dataIdent.get(3)).start();// 电压-A相
//					new Sender(equId, address, DataIdentify2007.dataIdent.get(4)).start();
//					new Sender(equId, address, DataIdentify2007.dataIdent.get(5)).start();

					new Sender(equId, address, DataIdentify2007.dataIdent.get(6)).start();// 电流-A相
//					new Sender(equId, address, DataIdentify2007.dataIdent.get(7)).start();
//					new Sender(equId, address, DataIdentify2007.dataIdent.get(8)).start();
//
					new Sender(equId, address, DataIdentify2007.dataIdent.get(9)).start();// 有功功率
					new Sender(equId, address, DataIdentify2007.dataIdent.get(13)).start();// 无功功率
					new Sender(equId, address, DataIdentify2007.dataIdent.get(17)).start();// 视在功率
//
					new Sender(equId, address, DataIdentify2007.dataIdent.get(0)).start();// 实时电量（组合有功）
					new Sender(equId, address, DataIdentify2007.dataIdent.get(1)).start();// 正向实时电量
					new Sender(equId, address, DataIdentify2007.dataIdent.get(2)).start();// 反向实时电量
				}
			}
			log.info("#发送线程数：[{}]，#已连接电表数：[{}]", runningEquIdSet.size(), Server.connectedEquId2Ctx.size());
			Thread.sleep(10000);// 每10s更新一下电表连接池，对应的采集作业线程
		}
	}

}
